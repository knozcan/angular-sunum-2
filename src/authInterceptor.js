(function () {
    angular.module("authInterceptor")
        .factory("authInterceptor", authInterceptor);

    function authInterceptor(){
        return {
            request: function(config) {
                var token = "my-encoded-token";

                config.token = token;

                return config;
            },
            response: function (response) {
                //if 401 delete session data redirect to /login
            }
        }
    }
})();