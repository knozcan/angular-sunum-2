(function () {
    angular.module("angularStart")
        .directive("todo", todo);

    function todo(){
        return {
            scope: {
                data: "=",
                onTodoDelete: "&"
            },
            templateUrl: "todo.template.html",
            link: function (scope) {
                scope.formData = {};
                scope.todoEditFormStyle = {display: "none"};

                scope.onTodoEdit = function (formData) {
                    scope.data = formData;
                    scope.formData = {};

                    scope.toggleTodoFormOpen();
                };

                scope.onTodoEditClick = function () {
                    scope.toggleTodoFormOpen();
                };

                scope.toggleTodoFormOpen = function () {
                    if (scope.todoEditFormStyle.display === "none") {
                        scope.todoEditFormStyle.display = {
                            display: "block"
                        }
                    } else {
                        scope.todoEditFormStyle = {
                            display: "none"
                        }
                    }
                };
            }
        }
    }
})();