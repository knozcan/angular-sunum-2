(function () {
    angular.module("angularStart")
        .controller("AngularStartController", AngularStartController);

    function AngularStartController($scope, $route) {
        $scope.title = "Hello World";
        $scope.todoList = $route.current.locals.todoList;
    }

})();