(function (angular){
    angular.module("angularStart", ["ngRoute"])
        .config(function ($routeProvider, $httpProvider) {

            $httpProvider.interceptors.push('authInterceptor');

            $routeProvider.when("/todos", {
                templateUrl: "todoView.template.html",
                //controller: "AngularStartController",
                resolve: {
                    todoList: function ($q, todoApi) {
                        return new $q(function (resolve, reject) {
                            todoApi.getTodos().then(function (data) {
                                resolve(data);
                            }, function (err) {
                                reject(err.message);
                            });
                        });
                    }
                }
            })
                .otherwise({
                    redirectTo: "/todos"
                });


        });

})(angular);
