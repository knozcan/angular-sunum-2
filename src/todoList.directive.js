(function () {
    angular.module("angularStart")
        .directive("todoList", todoList);

    function todoList(){
        return {
            scope: {
                data: "="
            },
            templateUrl: "todoList.template.html",
            link: function (scope){
                scope.editForm = {
                    todo: {}
                };

                scope.onTodoAdd = function () {
                    scope.data.push({
                        title: scope.todotmp
                    });

                    scope.todotmp = "";
                };

                scope.onTodoDelete = function (index) {
                    scope.data.splice(index, 1);
                };
            }
        }
    }
})();