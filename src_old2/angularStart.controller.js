(function () {
    angular.module("angularStart")
        .controller("AngularStartController", AngularStartController);

    function AngularStartController($scope) {
        $scope.title = "Hello World";

        $scope.todoList = [{
            title: "todo 1"
        }, {
            title: "todo 2"
        }, {
            title: "todo 3"
        }];
    }

})();