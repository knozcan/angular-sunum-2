(function () {
    angular.module("angularStart")
        .controller("AngularStartController", AngularStartController);

    function AngularStartController($scope, todoApi) {
        $scope.title = "Hello World";
        $scope.todoList = [];

        todoApi.getTodos().then(function (data) {
            $scope.todoList = data;
        }, function (err) {
            $scope.error = err.message;
        });

    }

})();