(function () {
    angular.module("angularStart")
        .factory("todoApi", todoApi);

    function todoApi($q){
        return {
            getTodos: function(){
                return new $q(function (resolve, reject) {
                    setTimeout(function (){
                        resolve([{
                            title: "todo 1"
                        }, {
                            title: "todo 2"
                        }, {
                            title: "todo 3"
                        }]);
                    }, 1000);
                });
            }
        }
    }
})();

//more code