(function () {
    angular.module("angularStart")
        .controller("AngularStartController", AngularStartController);

    function AngularStartController($scope) {
        $scope.title = "Hello World";
        $scope.editForm = {
            todo: {}
        };
        $scope.todoEditFormStyle = {display: "none"};

        $scope.todoList = [];

        $scope.onTodoAdd = function () {
            $scope.todoList.push({
                title: $scope.todotmp
            });

            $scope.todotmp = "";
        };

        $scope.onTodoDelete = function (index) {
            $scope.todoList.splice(index, 1);
        };

        $scope.onTodoEdit = function (index) {
            console.log(index);
        };

        $scope.onTodoEditClick = function () {
            $scope.toggleTodoFormOpen();
        };

        $scope.toggleTodoFormOpen = function () {
            if ($scope.todoEditFormStyle.display === "none") {
                $scope.todoEditFormStyle.display = {
                    display: "block"
                }
            } else {
                $scope.todoEditFormStyle = {
                    display: "none"
                }
            }
        }
    }

})();