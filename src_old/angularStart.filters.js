(function () {
    angular.module("angularStart")
        .filter("myCustomFilter", function () {
            return function (arr, value){
                var result = [];

                for(var i = 0;i < arr.length; i++){
                    if(arr[i].name === value || arr[i].surname === value){
                        result.push(arr[i]);
                    }
                }

                return result;
            }
        });
})();